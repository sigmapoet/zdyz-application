#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <linux/watchdog.h>

#define WDOG_DEV "/dev/watchdog"

int main(int argc, char **argv) {
    struct watchdog_info info;
    int timeout;
    int time;
    int fd;
    int op;
    int ret;

    if(argc != 2) {
        fprintf(stderr, "usage: %s <timeout>\n", argv[0]);
        exit(-1);
    }

    fd = open(WDOG_DEV, O_RDWR);
    if(fd < 0) {
        fprintf(stderr, "open error: %s: %s\n", WDOG_DEV, strerror(errno));
        exit(-1);
    }

    //打开之后看门狗计时器会开启，先停止它
    op = WDIOS_DISABLECARD;
    ret = ioctl(fd, WDIOC_SETOPTIONS, &op);
    if(ret < 0) {
        fprintf(stderr, "ioctl error: WDIOC_SETOPTIONS: %s\n", strerror(errno));
        close(fd);
        exit(-1);
    }

    timeout = atoi(argv[1]);
    if(timeout < 1) {
        timeout = 1;
    }
    printf("timeout: %ds\n", timeout);
    ret = ioctl(fd, WDIOC_SETTIMEOUT, &timeout);
    if(ret < 0) {
        fprintf(stderr, "ioctl error: WDIOC_SETTIMEOUT: %s\n", strerror(errno));
        close(fd);
        exit(-1);
    }

    op = WDIOS_ENABLECARD;
    ret = ioctl(fd, WDIOC_SETOPTIONS, &op);
    if(ret < 0) {
        fprintf(stderr, "ioctl error: WDIOC_SETOPTIONS: %s\n", strerror(errno));
        close(fd);
        exit(-1);
    }

    time = (timeout * 1000 - 100) * 1000;
    for(;;) {
        usleep(time);
        ioctl(fd, WDIOC_KEEPALIVE, NULL);   //喂狗
    }

}