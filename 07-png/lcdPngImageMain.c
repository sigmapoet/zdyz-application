#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <png.h>

static int width;
static int height;
static unsigned short *screenBase = NULL;
static unsigned long lineLen;
static unsigned int bpp;

static int showPNGImage(const char *filepath) {
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    FILE *pngFile = NULL;
    unsigned short *fbLineBuf = NULL;
    unsigned int minH, minW;
    unsigned int validBytes;
    unsigned int imageH, imageW;
    png_bytepp row_pointers = NULL;
    int i, j, k;

    //打开png文件
    pngFile = fopen(filepath, "r");
    if(pngFile == NULL) {
        perror("png file open error");
        return -1;
    }

    //分配和初始化png_ptr info_ptr
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) {
        fclose(pngFile);
        return -1;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        fclose(pngFile);
        return -1;
    }

    //设置错误返回点
    if(setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(pngFile);
        return -1;
    }

    //指定数据源
    png_init_io(png_ptr, pngFile);

    //读取png文件
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_ALPHA, NULL);
    imageH = png_get_image_height(png_ptr, info_ptr);
    imageW = png_get_image_width(png_ptr, info_ptr);
    printf("分辨率：%d * %d\n", imageW, imageH);

    //判断是否为RGB888格式
    if((8 != png_get_bit_depth(png_ptr, info_ptr)) &&
        (PNG_COLOR_TYPE_RGB != png_get_color_type(png_ptr, info_ptr))) {
        printf("Error: Not 8-bit depth or not RGB color\n");
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(pngFile);
        return -1;            
    }

    //判断图像和LCD屏哪个的分辨率更低
    if(imageW > width) {
        minW = width;
    }
    else {
        minW = imageW;
    }
    if(imageH > height) {
        minH = height;
    }
    else {
        minH = imageH;
    }
    validBytes = minW * bpp / 8;

    //读取解码后的数据
    fbLineBuf = malloc(validBytes);
    row_pointers = png_get_rows(png_ptr, info_ptr);

    unsigned int tmp = minW * 3;
    for(i = 0; i < minH; ++ i) {
        for(j = k = 0; j < tmp; j += 3, k ++) {
            fbLineBuf[k] = ((row_pointers[i][j] & 0xf8) << 8) | 
            ((row_pointers[i][j+1] & 0xfc) << 3) |
            ((row_pointers[i][j+2] & 0xf8) >> 3);
        }
        memcpy(screenBase, fbLineBuf, validBytes);
        screenBase += width;
    }

    //结束、销毁／释放内存
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    free(fbLineBuf);
    fclose(pngFile);

    return 0;
}

int main(int argc, char **argv) {
    struct fb_fix_screeninfo fbFInfo;
    struct fb_var_screeninfo fbVInfo;
    unsigned int screenSize;
    int fd;

    if(2 != argc) {
        fprintf(stderr, "usage: %d <png_file>\n", argv[0]);
        exit(-1);
    }

    fd = open("/dev/fb0", O_RDWR);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    ioctl(fd, FBIOGET_VSCREENINFO, &fbVInfo);
    ioctl(fd, FBIOGET_FSCREENINFO, &fbFInfo);

    lineLen = fbFInfo.line_length;
    bpp = fbVInfo.bits_per_pixel;
    screenSize = lineLen * fbVInfo.yres;
    width = fbVInfo.xres;
    height = fbVInfo.yres;

    screenBase = mmap(NULL, screenSize, PROT_WRITE, MAP_SHARED, fd, 0);
    if(MAP_FAILED == (void*)screenBase) {
        perror("mmap error");
        close(fd);
        exit(-1);
    }

    memset(screenBase, 0xff, screenSize);
    showPNGImage(argv[1]);

    munmap(screenBase, screenSize);
    close(fd);

    exit(0);
}