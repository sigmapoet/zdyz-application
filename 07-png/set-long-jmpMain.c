#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

static jmp_buf buf;

static void hello(void) {
    printf("hello world\n");
    longjmp(buf, 1);                //执行跳转，跳转到setjmp函数所设置的跳转点
    printf("Nice to meet you!\n");
}

int main() {
    if(0 == setjmp(buf)) {          //setjmp第一调用返回0
        printf("First return\n");
        hello();
    }
    else {
        printf("Second return\n");
    }

    exit(0);
}