#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <termios.h>

typedef struct uart_hardware_cfg {
    unsigned int baudrate;  //波特率
    unsigned char dbit;     //数据位
    char parity;            //奇偶校验
    unsigned char sbit;     //停止位
}uart_cfg_t;

static struct termios old_cfg;
static int fd;

static int uart_init(const char *device) {
    int ret;

    fd = open(device, O_RDWR | O_NOCTTY);
    if(fd < 0) {
        fprintf(stderr, "uart device open error: %s\n", strerror(errno));
        return -1;
    }

    ret = tcgetattr(fd, &old_cfg);
    if(ret < 0) {
        fprintf(stderr, "tcgetattr error: %s\n", strerror(errno));
        close(fd);
        return -1;
    }
    return 0;
}

static int uart_cfg(const uart_cfg_t *cfg) {
    struct termios new_cfg = {0};
    speed_t speed;
    int ret;

    //设置为原始模式
    cfmakeraw(&new_cfg);    
    //使能接收
    new_cfg.c_cflag |= CREAD;
    //设置波特率
    switch(cfg->baudrate) {
    case 1200:
        speed = B1200;
        break;
    case 1800:
        speed = B1800;
        break;
    case 2400:
        speed = B2400;
        break;
    case 4800:
        speed = B4800;
        break;
    case 9600:
        speed = B9600;
        break;
    case 19200:
        speed = B19200;
        break;
    case 38400:
        speed = B38400;
        break;
    case 57600:
        speed = B57600;
        break;
    case 115200:
        speed = B115200;
        break;
    case 230400:
        speed = B230400;
        break;
    case 460800:
        speed = B460800;
        break;
    case 500000:
        speed = B500000;
        break;
    default:
        speed = B115200;
        printf("default baudrate: 115200\n");
        break;
    }
    ret = cfsetspeed(&new_cfg, speed);
    if(ret < 0) {
        fprintf(stderr, "baudrate cfsetspeed error:%s\n", strerror(errno));
        return -1;
    }
    //设置数据位大小
    new_cfg.c_cflag &= ~CSIZE;
    switch(cfg->dbit) {
    case 5:
        new_cfg.c_cflag |= CS5;
        break;
    case 6:
        new_cfg.c_cflag |= CS6;
        break;
    case 7:
        new_cfg.c_cflag |= CS7;
        break;
    case 8:
        new_cfg.c_cflag |= CS8;
        break;
    default:
        new_cfg.c_cflag |= CS8;
        printf("default data bit size: 8\n");
        break;
    }
    //设置奇偶校验
    switch (cfg->parity) {
    case 'N': //无校验
        new_cfg.c_cflag &= ~PARENB;
        new_cfg.c_iflag &= ~INPCK;
        break;
    case 'O': //奇校验
        new_cfg.c_cflag |= (PARODD | PARENB);
        new_cfg.c_iflag |= INPCK;
        break;
    case 'E': //偶校验
        new_cfg.c_cflag |= PARENB;
        new_cfg.c_cflag &= ~PARODD; /* 清除 PARODD 标志，配置为偶校验 */
        new_cfg.c_iflag |= INPCK;
    break;
        default: //默认配置为无校验
        new_cfg.c_cflag &= ~PARENB;
        new_cfg.c_iflag &= ~INPCK;
        printf("default parity: N\n");
        break;
    }
    //设置停止位
    switch (cfg->sbit) {
    case 1: //1 个停止位
        new_cfg.c_cflag &= ~CSTOPB;
        break;
    case 2: //2 个停止位
        new_cfg.c_cflag |= CSTOPB;
        break;
    default: //默认配置为 1 个停止位
        new_cfg.c_cflag &= ~CSTOPB;
        printf("default stop bit size: 1\n");
        break;
    }
    //将 MIN 和 TIME 设置为 0
    new_cfg.c_cc[VTIME] = 0;
    new_cfg.c_cc[VMIN] = 0;
    //清空缓冲区
    ret = tcflush(fd, TCIOFLUSH);
    if(ret < 0) {
        fprintf(stderr, "tcflush error: %s\n", strerror(errno));
        return -1;
    }
    //写入配置，使生效
    ret = tcsetattr(fd, TCSANOW, &new_cfg);
    if (ret < 0) {
        fprintf(stderr, "tcsetattr error: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

static void show_help(const char *app) {
    printf("Usage: %s [选项]\n"
    "\n 必选选项:\n"
    " --dev=DEVICE 指定串口终端设备名称, 譬如--dev=/dev/ttymxc2\n"
    " --type=TYPE 指定操作类型, 读串口还是写串口, 譬如--type=read(read 表示读、write 表示写、其它值无效)\n"
    "\n 可选选项:\n"
    " --brate=SPEED 指定串口波特率, 譬如--brate=115200\n"
    " --dbit=SIZE 指定串口数据位个数, 譬如--dbit=8(可取值为: 5/6/7/8)\n"
    " --parity=PARITY 指定串口奇偶校验方式, 譬如--parity=N(N 表示无校验、O 表示奇校验、E 表示偶校验)\n"
    " --sbit=SIZE 指定串口停止位个数, 譬如--sbit=1(可取值为: 1/2)\n"
    " --help 查看本程序使用帮助信息\n\n", app);
}

//信号处理函数，当串口有数据可读时，会跳转到该函数执行
static void io_handler(int sig, siginfo_t *info, void *context) {
    unsigned char buf[10] = {0};
    int ret;

    if(SIGRTMIN != sig)
        return;
    if(POLL_IN == info->si_code) {
        ret = read(fd, buf, 8);
        printf("[");
        for(int i = 0; i < ret; ++ i) {
            printf("0x%hhx ", buf[i]);
        }
        printf("]\n");
    }
}

//异步I/O初始化函数
static void async_io_init() {
    struct sigaction sigatn;
    int flag;

    // 使能异步IO
    flag = fcntl(fd, F_GETFL);
    flag |= O_ASYNC;
    fcntl(fd, F_SETFL, flag);
    // 设置异步IO的所有者
    fcntl(fd, F_SETOWN, getpid());
    // 指定实时信号SIGRTMIN作为异步IO通知信号
    fcntl(fd, F_SETSIG, SIGRTMIN);
    // 为实时信号SIGRTMIN注册信号处理函数
    sigatn.sa_sigaction = io_handler;
    sigatn.sa_flags = SA_SIGINFO;
    sigemptyset(&sigatn.sa_mask);
    sigaction(SIGRTMIN, &sigatn, NULL);
}

int main(int argc, char *argv[]) {
    uart_cfg_t cfg = {0};
    char *device = NULL;
    int rw_flag = -1;
    int ret = 0;
    unsigned char writeBuf[10] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
    
    for(int i = 1; i < argc; ++ i) {
        if(!strncmp("--dev==", argv[i], 6)) {
            device = &argv[i][6];
        }
        else if(!strncmp("--brate=", argv[i], 8)) {
            cfg.baudrate = atoi(&argv[i][8]);
        }
        else if(!strncmp("--dbit=", argv[i], 7)) {
            cfg.dbit = atoi(&argv[i][7]);
        }
        else if(!strncmp("--parity=", argv[i], 9)) {
            cfg.parity = argv[i][9];
        }
        else if(!strncmp("--sbit=", argv[i], 7)) {
            cfg.sbit = atoi(&argv[i][7]);
        }
        else if(!strncmp("--type=", argv[i], 7)) {
            if(!strcmp("read", &argv[i][7])) {
                rw_flag = 0;
            }
            else if(!strcmp("write", &argv[i][7])) {
                rw_flag = 1;
            }
        }
        else if(!strcmp("--help", argv[i])) {
            show_help(argv[0]);
            exit(0);
        }        
    }

    if(device == NULL || rw_flag == -1) {
        fprintf(stderr, "Error:the device and read|write type must be set!\n");
        show_help(argv[0]);
        exit(-1);
    }

    ret = uart_init(device);
    if(ret) {
        fprintf(stderr, "uart_init error\n");
        exit(-1);
    }

    ret = uart_cfg(&cfg);
    if(ret) {
        tcsetattr(fd, TCSANOW, &old_cfg);
        close(fd);
        fprintf(stderr, "uart_cfg error\n");
        exit(-1);
    }

    switch(rw_flag) {
    case 0:

        break;
    case 1:

        break;
    }

    tcsetattr(fd, TCSANOW, &old_cfg);
    close(fd);
    return 0;
}







