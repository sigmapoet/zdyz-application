#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <jpeglib.h>

typedef struct brg888_color {
    unsigned char red;
    unsigned char green;
    unsigned char blue;
}__attribute__ ((packed)) bgr888_t;

static int width;
static int height;
static unsigned short* screenBase;
static unsigned long lineLen;           //LCD一行的长度（单位为字节)
static unsigned int bpp;                //像素深度

static int showJPEGImage(const char* filepath) {
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    FILE *jpegFile = NULL;
    bgr888_t *jpegLineBuf = NULL;       //行缓冲区，用于存储从jpeg文件中解压出来的一行图像数据
    unsigned short *fbLineBuf = NULL;   //行缓冲区，用于存储写入到lcd显存的一行数据
    unsigned int minH, minW;
    unsigned int validBytes;
    int i;

    //绑定默认错误处理函数
    cinfo.err = jpeg_std_error(&jerr);
    
    //打开jpeg/jpg图像文件
    jpegFile = fopen(filepath, "r");
    if(jpegFile == NULL) {
        perror("jpeg/jpg file open error");
        return -1;
    }

    //创建jpeg解码对象
    jpeg_create_decompress(&cinfo);

    //指定图像文件
    jpeg_stdio_src(&cinfo, jpegFile);

    //读取图像信息
    jpeg_read_header(&cinfo, TRUE);
    printf("jpeg图像大小：%d * %d\n", cinfo.image_width, cinfo.image_height);

    //设置解码参数
    cinfo.out_color_space = JCS_RGB;    //默认为JCS_RGB
    //cinfo.scale_num = 1;
    //cinfo.scale_denom = 2;            //上述两行是图像缩放操作参数，如要将图片输出为原图的1/2，则cinfo.scale_num=1,cinfo.scale_denom=2

    //开始解码图像
    jpeg_start_decompress(&cinfo);

    //为缓冲区分配内存空间
    jpegLineBuf = malloc(cinfo.output_components * cinfo.output_width);
    fbLineBuf = malloc(lineLen);

    //判断图像和LCD屏哪个的分辨率更低
    if(cinfo.output_width > width) {
        minW = width;
    }
    else {
        minW = cinfo.output_width; 
    }
    if(cinfo.output_height > height) {
        minH = height;
    }
    else {
        minH = cinfo.output_height;
    }

    //读取数据
    validBytes = minW * bpp / 8;        //一行的有效字节数，表示真正写入到LCD显存的一行数据的大小
    while(cinfo.output_scanline < minH) {
        jpeg_read_scanlines(&cinfo, (unsigned char**)&jpegLineBuf, 1);      //每次读取一行数据

        //BGR888->RGB565
        for(i = 0; i < minW; ++ i) {
            fbLineBuf[i] = ((jpegLineBuf[i].red & 0xf8) << 8) | 
                        ((jpegLineBuf[i].green & 0xfc) << 3) |
                        ((jpegLineBuf[i].blue & 0xf8) >> 3);
        }
        memcpy(screenBase, fbLineBuf, validBytes);
        screenBase += width;
    }

    //完成解码
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    //关闭文件、释放内存
    fclose(jpegFile);
    free(fbLineBuf);
    free(jpegLineBuf);

    return 0;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        fprintf(stderr, "usage: %s <jpeg_file>\n", argv[0]);
        exit(-1);
    }

    struct fb_fix_screeninfo fbFInfo;
    struct fb_var_screeninfo fbVInfo;
    int fd;
    int ret;
    unsigned int screenSize;

    fd = open("/dev/fb0", O_RDWR);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    ioctl(fd, FBIOGET_VSCREENINFO, &fbVInfo);
    ioctl(fd, FBIOGET_FSCREENINFO, &fbFInfo);

    lineLen = fbFInfo.line_length;
    bpp = fbVInfo.bits_per_pixel;
    screenSize = lineLen * fbVInfo.yres;
    width = fbVInfo.xres;
    height = fbVInfo.yres;

    screenBase = mmap(NULL, screenSize, PROT_WRITE, MAP_SHARED, fd, 0);
    if((void*)screenBase == MAP_FAILED) {
        perror("mmap error");
        close(fd);
        exit(-1);
    }

    memset(screenBase, 0xff, screenSize);
    showJPEGImage(argv[1]);

    munmap(screenBase, screenSize);
    close(fd);
    exit(0);
}