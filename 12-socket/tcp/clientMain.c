#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define SERVER_PORT 6002
#define SERVER_IP "192.168.137.2"

int main() {
    struct sockaddr_in server_addr = {0};
    char buf[512];
    int sockfd;
    int ret;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) {
        perror("socket error");
        exit(-1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr);

    ret = connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(ret < 0) {
        perror("connect error");
        close(sockfd);
        exit(-1);
    }

    printf("服务器连接成功...\n\r");

    for(;;) {
        memset(buf, 0x0, sizeof(buf));

        printf("please enter a string:");
        fgets(buf, sizeof(buf), stdin);

        ret = send(sockfd, buf, strlen(buf), 0);
        if(ret < 0) {
            perror("send error");
            break;
        }

        if(strncmp(buf, "exit", 4) == 0) {
            break;
        }
    }
    close(sockfd);
    exit(0);
}
