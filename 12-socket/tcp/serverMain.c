#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define SERVER_PORT 8888

int main() {
    struct sockaddr_in server_addr = {0};
    struct sockaddr_in client_addr = {0};
    char ip_str[20] = {0};
    int sockfd, connfd;
    int addrlen = sizeof(client_addr);
    char recvbuf[512];
    int ret;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) {
        perror("socket error");
        exit(-1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);    //htonl htons  区分大小端
    server_addr.sin_port = htons(SERVER_PORT);

    ret = bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(ret < 0) {
        perror("bind error");
        close(sockfd);
        exit(-1);
    }

    ret = listen(sockfd, 50);
    if(ret < 0) {
        perror("listen error");
        close(sockfd);
        exit(-1);
    }

    connfd = accept(sockfd, (struct sockaddr*)&client_addr, &addrlen);
    if(connfd < 0) {
        perror("accept error");
        close(sockfd);
        exit(-1);
    }

    printf("有客户端接入...\n\r");
    inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, ip_str, sizeof(ip_str));
    printf("客户端主机的IP地址：%s\n\r", ip_str);
    printf("客户端进程的端口号：%d\n\r", client_addr.sin_port);

    for(;;) {
        memset(recvbuf, 0x0, sizeof(recvbuf));

        ret =recv(connfd, recvbuf, sizeof(recvbuf), 0);
        if(ret <= 0) {
            perror("recv error");
            close(connfd);
            break;
        }

        printf("from client: %s\n\r", recvbuf);

        if(strncmp("exit", recvbuf, 4) == 0) {
            printf("server exit...\n\r");
            close(connfd);
            break;
        }
    }

    close(sockfd);
    exit(0);
}