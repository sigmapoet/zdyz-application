#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>

int main(int argc, char **argv) {
    struct input_absinfo info;
    int fd = -1;
    int max_slots;
    int ret;

    if(argc != 2) {
        printf("usage : %s <input-dev> \n", argv[0]);
        exit(-1);
    }

    fd = open(argv[1], O_RDONLY);
    if(fd == -1) {
        perror("open error");
        exit(-1);
    }

    ret = ioctl(fd, EVIOCGABS(ABS_MT_SLOT), &info);
    if(ret < 0) {
        perror("ioctl error");
        close(fd);
        exit(-1);
    }

    max_slots = info.maximum + 1 - info.minimum;
    printf("max_slots : %d\n", max_slots);

    close(fd);
    exit(0);
}