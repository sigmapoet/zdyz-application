#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char ** argv) {
    if(argc != 2) {
        printf("Usage:\n");
        printf("for example : %s /dev/input/event0\n", argv[0]);
        exit(-1);
    }

    int ret;
    int fd;
    char devPath[100];
    struct input_event inputEvent = {0};

    memset(devPath, 0, sizeof(devPath));
    sprintf(devPath, "%s", argv[1]);
    printf("dev path : %s\n", devPath);

    fd = open(devPath, O_RDONLY);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    for(;;) {
        ret = read(fd, &inputEvent, sizeof(struct input_event));
        if(ret != sizeof(struct input_event)) {
            perror("input event read error");
            close(fd);
            exit(-1);
        }

        printf("Type: %d, Code : %d, Value: %d\n", inputEvent.type, inputEvent.code, inputEvent.value);
        switch(inputEvent.value) {
            case 0:
                printf("code<%d>: 松开\n", inputEvent.code);
                break;
            case 1:
                printf("code<%d>: 按下\n", inputEvent.code);
                break;
            case 2:
                printf("code<%d>: 长按\n", inputEvent.code);
                break;
        }
    }

    close(fd);
    exit(0);
}