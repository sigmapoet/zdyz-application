#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int gpio_config(char* gpioPath, char* fileName, char* value) {
    int fd;
    int len;
    int ret;
    char filePath[100];

    sprintf(filePath, "%s/%s", gpioPath, fileName);
    fd = open(filePath, O_WRONLY);
    if(fd == -1) {
        perror("open error");
        return -1;
    }

    len = strlen(value);
    ret = write(fd, value, len);
    if(ret != len) {
        perror("write error");
        close(fd);
        return -1;
    }

    close(fd);
    return 0;
}

int main(int argc, char** argv) {
    if(argc != 3) {
        printf("usage: %s <gpio> <value>\n", argv[0]);
        printf("For example:\n");
        printf("./gpioOutputMain 0 1\n");
        printf("gpio0 output 1\n");
        exit(-1);
    }

    char gpioPath[100];

    sprintf(gpioPath, "/sys/class/gpio/gpio%s", argv[1]);
    if(access(gpioPath, F_OK)) {
        int fd;
        int len;
        int ret;

        fd = open("/sys/class/gpio/export", O_WRONLY);
        if(fd == -1) {
            perror("open error");
            exit(-1);
        }

        len = strlen(argv[1]);
        ret = write(fd, argv[1], len);
        if(ret != len) {
            perror("write error");
            close(fd);
            exit(-1);
        }

        printf("export gpio%s successfully!\n", argv[1]);
        close(fd);
    }

    if(gpio_config(gpioPath, "direction", "out") == -1) {
        printf("write out > direction failed\n");
        exit(-1);
    }

    if(gpio_config(gpioPath, "active_low", "0") == -1) {
        printf("write 0 > active_low failed\n");
        exit(-1);
    }

    if(gpio_config(gpioPath, "value", argv[2]) == -1) {
        printf("write %s > value failed\n", argv[2]);
        exit(-1);
    }
    exit(0);
}