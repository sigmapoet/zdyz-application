#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int gpio_config(char* gpioPath, char* fileName, char* value) {
    int fd;
    int len;
    int ret;
    char filePath[100];

    sprintf(filePath, "%s/%s", gpioPath, fileName);
    fd = open(filePath, O_WRONLY);
    if(fd == -1) {
        perror("open error");
        return -1;
    }

    len = strlen(value);
    ret = write(fd, value, len);
    if(ret != len) {
        perror("write error");
        close(fd);
        return -1;
    }

    close(fd);
    return 0;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("usage: %s <gpio>\n", argv[0]);
        printf("For example:\n");
        printf("./gpioInputMain 0\n");
        printf("read value of gpio0\n");
        exit(-1);
    }

    int fd;
    int len;
    int ret;
    char val;
    char gpioPath[100];
    char gpioValuePath[100];

    sprintf(gpioPath, "/sys/class/gpio/gpio%s", argv[1]);
    sprintf(gpioValuePath, "%s/value", gpioPath);
  
    if(access(gpioPath, F_OK)) {
        fd = open("/sys/class/gpio/export", O_WRONLY);
        if(fd == -1) {
            perror("open error");
            exit(-1);
        }

        len = strlen(argv[1]);
        ret = write(fd, argv[1], len);
        if(ret != len) {
            perror("write error");
            close(fd);
            exit(-1);
        }

        printf("export gpio%s successfully!\n", argv[1]);
        close(fd);
    }

    if(gpio_config(gpioPath, "direction", "in") == -1) {
        printf("write in > direction failed\n");
        exit(-1);
    }
    if(gpio_config(gpioPath, "active_low", "0") == -1) {
        printf("write 0 > active_low failed\n");
        exit(-1);
    }
    if(gpio_config(gpioPath, "edge", "none") == -1) {
        printf("write none > edge failed\n");
        exit(-1);
    }

    fd = open(gpioValuePath, O_RDONLY);
    if(fd == -1) {
        perror("open error");
        exit(-1);
    }
    
    ret = read(fd, &val, 1);
    if(ret < 0) {
        perror("read error");
        close(fd);
        exit(-1);
    }   
    printf("val : %c\n", val);

    close(fd);
    exit(0);
}