#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <linux/fb.h>

#define FB_DEV "/dev/fb0"
#define FRAMEBUFFER_COUNT 3

typedef struct cameraFormat {
    unsigned char description[32];
    unsigned int pixelformat;
}cam_fmt;

typedef struct camBufInfo {
    unsigned short *start;
    unsigned long length;
}cam_buf_info;

int width;
int height;
unsigned short *screenBase = NULL;
int fb_fd = -1;
int v4l2_fd = -1;
cam_buf_info bufInfos[FRAMEBUFFER_COUNT];
cam_fmt camFmts[10];
int frmWidth, frmHeight;

int fb_dev_init() {
    struct fb_var_screeninfo fbVInfo = {0};
    struct fb_fix_screeninfo fbFInfo = {0};
    unsigned long screenSize;

    fb_fd = open(FB_DEV, O_RDWR);
    if(fb_fd == -1) {
        perror("fb_fd file open error");
        return -1;
    }

    ioctl(fb_fd, FBIOGET_FSCREENINFO, &fbFInfo);
    ioctl(fb_fd, FBIOGET_VSCREENINFO, &fbVInfo);

    screenSize = fbFInfo.line_length * fbVInfo.yres;
    width = fbVInfo.xres;
    height = fbVInfo.yres;
    printf("LCD屏幕分辨率：%d * %d\n", width, height);
    printf("LCD屏幕一行字节数：%d\n", fbFInfo.line_length);
    screenBase = mmap(NULL, screenSize, PROT_READ|PROT_WRITE, MAP_SHARED, fb_fd, 0);
    if(MAP_FAILED == (void*)screenBase) {
        perror("mmap error");
        close(fb_fd);
        return -1;
    }

    memset(screenBase, 0xff, screenSize);
    return 0;
}

int v4l2_dev_init(const char *device) {
    struct v4l2_capability cap = {0};

    v4l2_fd = open(device, O_RDWR);
    if(v4l2_fd == -1) {
        perror("v4l2_fd file open error");
        return -1;
    }

    ioctl(v4l2_fd, VIDIOC_QUERYCAP, &cap);
    if(!(V4L2_CAP_VIDEO_CAPTURE & cap.capabilities)) {
        fprintf(stderr, "Error: %s: No capture video device\n", device);
        close(v4l2_fd);
        return -1;
    }

    return 0;
}

void v4l2_enum_formats() {
    struct v4l2_fmtdesc fmtdesc = {0};

    fmtdesc.index = 0;
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    while(0 == ioctl(v4l2_fd, VIDIOC_ENUM_FMT, &fmtdesc)) {
        camFmts[fmtdesc.index].pixelformat = fmtdesc.pixelformat;
        strcpy(camFmts[fmtdesc.index].description, fmtdesc.description);
        fmtdesc.index ++;
    }
}

void v4l2_print_formats() {
    struct v4l2_frmsizeenum frmsize = {0};
    struct v4l2_frmivalenum frmival = {0};

    frmsize.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    frmival.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    for(int i = 0; camFmts[i].pixelformat; i ++) {
        printf("format<0x%x>, description<%s>\n", camFmts[i].pixelformat, camFmts[i].description);
        frmsize.index = 0;
        frmsize.pixel_format = camFmts[i].pixelformat;
        frmival.pixel_format = camFmts[i].pixelformat;
        while(0 == ioctl(v4l2_fd, VIDIOC_ENUM_FRAMESIZES, &frmsize)) {
            printf("size<%d*%d>", frmsize.discrete.width, frmsize.discrete.height);
            frmsize.index ++;

            frmival.index = 0;
            frmival.width = frmsize.discrete.width;
            frmival.height = frmsize.discrete.height;
            while(0 == ioctl(v4l2_fd, VIDIOC_ENUM_FRAMEINTERVALS, &frmival)) {
                printf("<%dfps>", frmival.discrete.denominator / frmival.discrete.numerator);
                frmival.index ++;
            }
            printf("\n");
        }
        printf("\n");
    }
}

int v4l2_set_format() {
    struct v4l2_format fmt = {0};
    struct v4l2_streamparm streamparm = {0};

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = width;
    fmt.fmt.pix.height = height;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB565;
    if(0 > ioctl(v4l2_fd, VIDIOC_S_FMT, &fmt)) {
        fprintf(stderr, "ioctl error: VIDIOC_S_FMT: %s\n", strerror(errno));
        return -1;
    }

    if(V4L2_PIX_FMT_RGB565 != fmt.fmt.pix.pixelformat) {
        fprintf(stderr, "Error: the device does not support RGB565 format\n");
        return -1;
    }

    frmWidth = fmt.fmt.pix.width;
    frmHeight = fmt.fmt.pix.height;
    printf("视频大小<%d * %d>\n", frmWidth, frmHeight);

    streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ioctl(v4l2_fd, VIDIOC_G_PARM, &streamparm);

    if(V4L2_CAP_TIMEPERFRAME & streamparm.parm.capture.capability) {
        streamparm.parm.capture.timeperframe.numerator = 1;
        streamparm.parm.capture.timeperframe.denominator = 30;
        if(0 > ioctl(v4l2_fd, VIDIOC_S_PARM, &streamparm)) {
            fprintf(stderr, "ioctl error: VIDIOC_S_PARM: %s\n", strerror(errno));
            return -1;
        }
    }

    return 0;
}

int v4l2_init_buffer() {
    struct v4l2_requestbuffers reqbuf = {0};
    struct v4l2_buffer buf = {0};

    reqbuf.count = FRAMEBUFFER_COUNT;
    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuf.memory = V4L2_MEMORY_MMAP;
    if(0 > ioctl(v4l2_fd, VIDIOC_REQBUFS, &reqbuf)) {
        fprintf(stderr, "ioctl error: VIDIOC_REQBUFS: %s\n", strerror(errno));
        return -1;
    }
    
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    for(buf.index = 0; buf.index < FRAMEBUFFER_COUNT; buf.index ++) {
        ioctl(v4l2_fd, VIDIOC_QUERYBUF, &buf);
        bufInfos[buf.index].length = buf.length;
        bufInfos[buf.index].start = mmap(NULL, buf.length, \
                                    PROT_READ|PROT_WRITE, MAP_SHARED, \
                                    v4l2_fd, buf.m.offset);
        if(MAP_FAILED == bufInfos[buf.index].start) {
            perror("mmap error");
            return -1;
        }
        printf("每一帧缓冲的长度:%ld\n", bufInfos[buf.index].length);
    }

    for(buf.index = 0; buf.index < FRAMEBUFFER_COUNT; buf.index ++) {
        if(0 > ioctl(v4l2_fd, VIDIOC_QBUF, &buf)) {
            fprintf(stderr, "ioctl error: VIDIOC_QBUF: %s\n", strerror(errno));
            return -1;
        }
    }

    return 0;
}

int v4l2_stream_on() {
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(0 > ioctl(v4l2_fd, VIDIOC_STREAMON, &type)) {
        fprintf(stderr, "ioctl error: VIDIOC_STREAMON: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

void v4l2_read_data() {
    struct v4l2_buffer buf = {0};
    unsigned short *base;
    unsigned short *start;
    int minW, minH;
    int j;

    if(width > frmWidth) {
        minW = frmWidth;
    }
    else {
        minW = width;
    }
    if(height > frmHeight) {
        minH = frmHeight;
    }
    else {
        minH = height;
    }
    printf("minW: %d, minH: %d\n", minW, minH);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    for(;;) {
        for(buf.index = 0; buf.index < FRAMEBUFFER_COUNT; ++ buf.index) {
            ioctl(v4l2_fd, VIDIOC_DQBUF, &buf);
            for(j = 0, base = screenBase, start = bufInfos[buf.index].start;\
                j < minH; ++ j) {
                    memcpy(base, start, minW*2);
                    base += width;
                    start += frmWidth;
                }
            ioctl(v4l2_fd, VIDIOC_QBUF, &buf);
        }
    }
}


int main(int argc, char **argv) {
    if(2 != argc) {
        fprintf(stderr, "Usage: %s <videv_dev>\n", argv[0]);
        exit(-1);
    }

    int ret;

    ret = fb_dev_init();
    if(ret) {
        fprintf(stderr, "fd dev init failed\n");
        exit(-1);
    }
    
    ret = v4l2_dev_init(argv[1]);
    if(ret) {
        fprintf(stderr, "v4l2 dev init failed\n");
        exit(-1);
    }

    v4l2_enum_formats();
    v4l2_print_formats();

    ret = v4l2_set_format();
    if(ret) {
        fprintf(stderr, "v4l2 set format failed\n");
        exit(-1);
    }

    ret = v4l2_init_buffer();
    if(ret) {
        fprintf(stderr, "v4l2 init buffer failed\n");
        exit(-1);
    }

    ret = v4l2_stream_on();
    if(ret) {
        fprintf(stderr, "v4l2 stream on failed\n");
        exit(-1);
    }

    v4l2_read_data();

    exit(0);
}