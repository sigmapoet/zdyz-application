#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define BRIGHTNESS "/sys/class/leds/sys-led/brightness"
#define TRIGGER    "/sys/class/leds/sys-led/trigger"

int main(int argc, char** argv) {
  if(argc < 2) {
    printf("Usage: %s on/off\n\r", argv[0]);
    printf("Usage: %s trigger heartbeat/none\n\r", argv[0]);
    exit(-1);
  }

  if(argc == 2) {
    int fd_brightness;
    int ret;
    
    fd_brightness = open(BRIGHTNESS, O_WRONLY);
    if(fd_brightness == -1) {
      perror("open error");
      exit(-1);
    }
    if(!strcmp("on", argv[1])) {
      ret = write(fd_brightness, "1", strlen("1"));
      if(ret != strlen("1")) {
        perror("write error");
        goto err1;
      }
      
    }
    else if(!strcmp("off", argv[1])) {
      ret = write(fd_brightness, "0", strlen("0"));
      if(ret != strlen("0")) {
        perror("write error");
        goto err1;
      }
    }
  err1:
    close(fd_brightness);
  }
  else if(argc == 3) {
    int fd_trigger;
    int ret;

    fd_trigger = open(TRIGGER, O_WRONLY);
    if(fd_trigger == -1) {
      perror("open error");
      exit(-1);
    }
    //printf("sizeof(argv[2]) : %d\n\r", sizeof(argv[2]));
    ret = write(fd_trigger, argv[2], strlen(argv[2]));//pay attention to "sizeof(argv[2])" and "strlen(argv[2])"
    if(ret != strlen(argv[2])) {
      perror("write error");
      goto err2;
    }
  err2:
    close(fd_trigger);
  }

  exit(0);
}
