#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/fb.h>



int main(int argc, char** argv) {
    if(argc != 2) {
        fprintf(stderr, "usage: %s /dev/fb0\n", argv[0]);
        exit(-1);
    }

    int fd;
    char devPath[100];
    struct fb_var_screeninfo fbVinfo;
    struct fb_fix_screeninfo fbFinfo;

    memset(devPath, 0, sizeof(devPath));
    sprintf(devPath, "%s", argv[1]);

    fd = open(devPath, O_RDONLY);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    ioctl(fd, FBIOGET_VSCREENINFO, &fbVinfo);
    ioctl(fd, FBIOGET_FSCREENINFO, &fbFinfo);

    printf("分辨率: %d * %d\n", fbVinfo.xres, fbVinfo.yres);
    printf("一行的字节数: %d\n", fbFinfo.line_length);

    close(fd);
    exit(0);
}