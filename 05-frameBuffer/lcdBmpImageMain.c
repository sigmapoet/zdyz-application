#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <linux/fb.h>
#include <sys/mman.h>

// bmp file header struct
typedef struct {
    unsigned char type[2];
    unsigned int size;
    unsigned short reserved1;
    unsigned short reserved2;
    unsigned int offset;
}__attribute__ ((packed)) bmp_file_header;

// bmp info header struct
typedef struct {
    unsigned int size;
    int width;
    int height;
    unsigned short planes;
    unsigned short bpp;
    unsigned int compression;
    unsigned int image_size;
    int x_pels_per_meter;
    int y_pels_per_meter;
    unsigned int clr_used;
    unsigned int clr_omportant;
}__attribute__ ((packed)) bmp_info_header;

//global variables
static int width;
static int height;
static unsigned short *screenBase = NULL;
static unsigned long lineLen;

static int showBMPImage(const char *imagePath) {
    bmp_file_header file_h;
    bmp_info_header info_h;
    unsigned short *lineBuf = NULL;
    unsigned long lineBytes;
    unsigned int minH, minBytes;
    int fd = -1;
    int ret, j;

    fd = open(imagePath, O_RDONLY);
    if(fd == -1) {
        perror("bmp file open error");
        return -1;
    }

    ret = read(fd, &file_h, sizeof(bmp_file_header));
    if(ret != sizeof(bmp_file_header)) {
        perror("bmp file header read error");
        close(fd);
        return -1;
    }

    ret = memcmp(file_h.type, "BM", 2);
    if(ret) {
        fprintf(stderr, "file type is not BMP\n");
        close(fd);
        return -1;
    }

    ret = read(fd, &info_h, sizeof(bmp_info_header));
    if(ret != sizeof(bmp_info_header)) {
        perror("bmp info header read error");
        close(fd);
        return -1;
    }

    printf("文件大小：%d\n", file_h.size);
    printf("位图数据偏移量：%d\n", file_h.offset);
    printf("位图信息头大小：%d\n", info_h.size);
    printf("图像分辨率：%d * %d\n", info_h.width, info_h.height);
    printf("像素深度：%d\n", info_h.bpp);

    ret = lseek(fd, file_h.offset, SEEK_SET);
    if(ret == -1) {
        perror("lseek error");
        close(fd);
        return -1;
    }

    lineBytes = info_h.width * info_h.bpp / 8;
    lineBuf = malloc(lineBytes);
    if(lineBuf == NULL) {
        fprintf(stderr, "malloc error\n");
        close(fd);
        return -1;
    }

    if(lineLen > lineBytes) {
        minBytes = lineBytes;
    }
    else {
        minBytes = lineLen;
    }

    if(info_h.height > 0) {
        if(info_h.height > height) {
            minH = height;
            lseek(fd, (info_h.height - height) * lineBytes, SEEK_CUR);
            screenBase += width * (height - 1);
        }
        else {
            minH = info_h.height;
            screenBase += width * (info_h.height - 1);
        }
        for(j = minH; j > 0; screenBase -= width, j --) {
            read(fd, lineBuf, lineBytes);
            memcpy(screenBase, lineBuf, minBytes);
        }
    }
    else {
        int tmp = 0 - info_h.height;
        if(tmp > height) {
            minH = height;
        }
        else {
            minH = tmp;
        }
        for(j = 0; j < minH; j ++, screenBase += width) {
            read(fd, lineBuf, lineBytes);
            memcpy(screenBase, lineBuf, lineBytes);
        }
    }

    close(fd);
    free(lineBuf);

    return 0;
}

int main(int argc, char **argv) {
    struct fb_fix_screeninfo fbFInfo;
    struct fb_var_screeninfo fbVInfo;
    unsigned int screenSize;
    int fd;
    int ret;

    if(argc != 2) {
        fprintf(stderr, "usage: %s <bmp_file>\n", argv[0]);
        exit(-1);
    }

    fd = open("/dev/fb0", O_RDWR);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    ioctl(fd, FBIOGET_VSCREENINFO, &fbVInfo);
    ioctl(fd, FBIOGET_FSCREENINFO, &fbFInfo);

    screenSize = fbFInfo.line_length * fbVInfo.yres;
    lineLen = fbFInfo.line_length;
    width = fbVInfo.xres;
    height = fbVInfo.yres;

    screenBase = mmap(NULL, screenSize, PROT_WRITE, MAP_SHARED, fd, 0);
    if((void*)screenBase == MAP_FAILED) {
        perror("mmap error");
        close(fd);
        exit(-1);
    }

    memset(screenBase, 0xff, screenSize);
    showBMPImage(argv[1]);

    munmap(screenBase, screenSize);
    close(fd);

    exit(0);
}