#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/fb.h>

#define u32 unsigned int
#define argb8888_to_rgb565(color) ({\
    unsigned int temp = (color); \
    ((temp & 0xf80000ul)>>8) | \
    ((temp & 0xfc00ul)>>5) | \
    ((temp & 0xf8ul)>>3); \
    })

static void lcdFill(u32 startX, u32 endX, u32 startY, u32 endY, u32 color, unsigned short *screenBase, int width, int height) {
    if(endX >= width) {
        endX = width - 1;
    }
    if(endY >= height) {
        endY = height - 1;
    }

    unsigned short rgb565Color = argb8888_to_rgb565(color);
    unsigned long tmp;
    unsigned int x;

    printf("screenBase : %p\n", screenBase);
    printf("rgb565Color: %hu\n", rgb565Color);
    tmp = startY * width;
    for(; startY <= endY; startY ++, tmp+=width) {
        for(x = startX; x <= endX; x ++) {
            //printf("Points Color Changed\n");
            //printf("tmp + x : %ld\n", tmp+x);
            screenBase[tmp + x] = rgb565Color;
        }
    }

    return;
}

void draw(unsigned short *screeBase, int width, int height) {
    int w = height * 0.25;
    lcdFill(0, width-1, 0, height-1, 0x0, screeBase, width, height);
    lcdFill(0, w, 0, w, 0xff0000, screeBase, width, height);

    printf("Draw done\n");
    return;
}

int main(int argc, char** argv) {
    if(2 != argc) {
        fprintf(stderr, "usage: %s /dev/fb0\n", argv[0]);
        exit(-1);
    }

    struct fb_fix_screeninfo fbFInfo;
    struct fb_var_screeninfo fbVInfo;
    unsigned int screenSize;
    unsigned short *screeBase = NULL;
    int width;
    int height;
    int fd;
    char devPath[100];

    memset(devPath, 0, sizeof(devPath));
    sprintf(devPath, "%s", argv[1]);
    
    fd = open(devPath, O_RDWR);
    if(fd == -1) {
        perror("dev file open error");
        exit(-1);
    }

    ioctl(fd, FBIOGET_VSCREENINFO, &fbVInfo);
    ioctl(fd, FBIOGET_FSCREENINFO, &fbFInfo);

    screenSize = fbFInfo.line_length * fbVInfo.yres;  //一行的字节数*行数
    width = fbVInfo.xres;
    height = fbVInfo.yres;

    printf("分辨率: %d x %d\n", width, height);
    printf("screenSize : %u\n", screenSize);

    screeBase = mmap(NULL, screenSize, PROT_WRITE, MAP_SHARED, fd, 0);
    if((void*) screeBase == MAP_FAILED) {
        perror("mmap error");
        close(fd);
        exit(-1);
    }

    draw(screeBase, width, height);

    munmap(screeBase, screenSize);
    close(fd);
    exit(0);
}