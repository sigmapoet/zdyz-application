#include <stdio.h>
#include <stdlib.h>
#include <tslib.h>

int main(int argc, char **argv) {
    struct tsdev *ts = NULL;
    struct ts_sample sample;
    int pressure = 0;
    int ret = 0;

    ts = ts_setup(NULL, 0);
    if(ts == NULL) {
        fprintf(stderr, "ts_setup error");
        exit(-1);
    }

    for(;;) {
        ret = ts_read(ts, &sample, 1);
        if(ret < 0) {
            fprintf(stderr, "ts_read error");
            ts_close(ts);
            exit(-1);
        }    

        if(sample.pressure > 0) {
            if(pressure) {
                printf("移动(%d, %d)\n", sample.x, sample.y);
            }
            else {
                printf("按下(%d, %d)\n", sample.x, sample.y);
            }
        }
        else {
            printf("松开\n");
        }

        pressure = sample.pressure;
    }

    ts_close(ts);
    exit(0);
}