#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <math.h>

#include <wchar.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#define FB_DEV "/dev/fb0"

#define argb8888_to_rgb565(color) ({ \
            unsigned int temp = (color); \
            ((temp & 0xf80000ul) >> 8) | \
            ((temp & 0xfc00ul) >> 5) | \
            ((temp & 0xf8ul) >> 3); \
            })

static unsigned int width;
static unsigned int height;
static unsigned short *screenBase = NULL;
static unsigned long screenSize;
static int fd = -1;

static FT_Library library;
static FT_Face face;

static int fbDevInit() {
    struct fb_var_screeninfo fbVInfo;
    struct fb_fix_screeninfo fbFInfo;

    fd = open(FB_DEV, O_RDWR);
    if(fd == -1) {
        perror("dev file open error");
        return -1;
    }

    ioctl(fd, FBIOGET_FSCREENINFO, &fbFInfo);
    ioctl(fd, FBIOGET_VSCREENINFO, &fbVInfo);

    screenSize = fbFInfo.line_length * fbVInfo.yres;
    width = fbVInfo.xres;
    height = fbVInfo.yres;

    screenBase = mmap(NULL, screenSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if(MAP_FAILED == (void*)screenBase) {
        perror("mmap error");
        close(fd);
        return -1;
    }

    memset(screenBase, 0xff, screenSize);
    return 0;
}

static int freetypeInit(const char*font, int angle) {
    FT_Error error;
    FT_Vector pen;
    FT_Matrix matrix;
    float rad;

    //freetype 初始化
    FT_Init_FreeType(&library);

    //加载face对象
    error = FT_New_Face(library, font, 0, &face);
    if(error) {
        fprintf(stderr, "FT_New_Face error: %d\n", error);
        exit(-1);
    }

    //原点坐标
    pen.x = 0 * 64;
    pen.y = 0 * 64;

    //2x2 矩阵初始化
    rad = (1.0 * angle / 180) * M_PI;
#if 0   //非水平方向
    matrix.xx = (FT_Fixed)(cos(rad) * 0x10000L);
    matrix.xy = (FT_Fixed)(-sin(rad) * 0x10000L);
    matrix.yx = (FT_Fixed)(sin(rad) * 0x10000L);
    matrix.yy = (FT_Fixed)(cos(rad) * 0x10000L);
#endif

#if 1   //斜体 水平方向显示
    matrix.xx = (FT_Fixed)(cos(rad) * 0x10000L);
    matrix.xy = (FT_Fixed)(sin(rad) * 0x10000L);
    matrix.yx = (FT_Fixed)(0 * 0x10000L);
    matrix.yy = (FT_Fixed)(1 * 0x10000L);
#endif

    //设置
    FT_Set_Transform(face, &matrix, &pen);
    FT_Set_Pixel_Sizes(face, 35, 0);       //设置字体大小

    return 0;
}

static void lcdDrawChar(int x, int y, const wchar_t *str, unsigned int color) {
    unsigned short rgb565_color = argb8888_to_rgb565(color);
    FT_GlyphSlot slot = face->glyph;
    size_t len = wcslen(str);       //计算字符的个数
    long int temp;
    int n;
    int i, j, p, q;
    int maxX, maxY, startY, startX;

    //循环加载各个字符
    for(n = 0; n < len; ++ n) {
        //加载字形，转换得到位图数据
        if(FT_Load_Char(face, str[n], FT_LOAD_RENDER))
            continue;
        startY = y - slot->bitmap_top;
        if(startY < 0) {
            q = -startY;
            temp = 0;
            j = 0;
        }
        else {
            q = 0;
            temp = width * startY;
            j = startY;
        }

        maxY = startY + slot->bitmap.rows;
        if(maxY > (int)height) {
            maxY = height;
        }

        for(; j < maxY; ++ j, ++ q, temp += width) {
            startX = x + slot->bitmap_left;
            if(0 > startX) {
                p = -startX;
                i = 0;
            }
            else {
                p = 0;
                i = startX;
            }

            maxX = startX + slot->bitmap.width;
            if(maxX > (int)width) {
                maxX = width;
            }

            for(; i < maxX; ++ i, ++ p) {
                if(slot->bitmap.buffer[q * slot->bitmap.width + p]) {
                    screenBase[temp + i] = rgb565_color;
                }
            }
        }
        x += slot->advance.x / 64;
        y -= slot->advance.y / 64;
    }
}

int main(int argc, char **argv) {
    if(3 != argc) {
        fprintf(stderr, "usage: %s <font file address> <angle>\n", argv[0]);
        exit(-1);
    }
    //lcd初始化
    if(fbDevInit()) {
        exit(-1);
    }

    //freetype初始化
    if(freetypeInit(argv[1], atoi(argv[2]))) {
        exit(-1);
    }

    //显示中文
    int y = height * 0.25;

    lcdDrawChar(20, 100, L"人生如逆旅，我亦是行人", 0x000000);
    lcdDrawChar(20, y+100, L"莫愁前路无知己，天下谁人不识君", 0x9900FF);
    lcdDrawChar(20, 2*y+100, L"君不见黄河之水天上来，奔流到海不复回", 0xFF0099);
    lcdDrawChar(20, 3*y+100, L"君不见高堂明镜悲白发，朝如青丝暮成雪", 0x9932CC);
    //退出
    FT_Done_Face(face);
    FT_Done_FreeType(library);
    munmap(screenBase, screenSize);
    close(fd);

    exit(0);
}